############################### Install Docker ###################################
---
- name: Install docker
  hosts: all
  become: yes
  gather_facts: true

  vars:
    ansible_ssh_user: ansible_user
    ansible_ssh_pass: ansible_ssh_pass

  tasks:
    - name: Configuring network settings for Kube 
      file:
        path: "/etc/sysctl.d/k8s.conf"
        state: "touch"

    - name: Adding required settings for containerd
      blockinfile:
        path: "/etc/sysctl.d/k8s.conf"
        block: |
               net.ipv4.ip_forward = 1
               net.bridge.bridge-nf-call-iptables = 1
               net.bridge.bridge-nf-call-ip6tables = 1

    - name: Creating a configuration file for containerd, our container runtime
      file:
        path: "/etc/modules-load.d/containerd.conf"
        state: "touch"
      
    - name: Remove swapfile from /etc/fstab
      mount:
        name: "{{ item }}"
        fstype: swap
        state: absent
      with_items:
        - swap
        - none

    - name: Disable swap
      command: swapoff -a
      when: ansible_swaptotal_mb > 0

    - name: System update all packages
      yum: 
        name: '*'
        state: latest

    - name: Install Prequest
      yum:
        name: 
          - lvm2
          - vim
          - wget
          - net-tools
          - yum-utils
          - device-mapper-persistent-data
        state: latest

    - name: Add Docker repo
      get_url:
        url: https://download.docker.com/linux/centos/docker-ce.repo
        dest: /etc/yum.repos.d/docer-ce.repo
      become: yes

    - name: Enable Docker Test nightly repo
      ini_file:
        dest: /etc/yum.repos.d/docer-ce.repo
        section: 'docker-ce-nightly'
        option: enabled
        value: 0
      become: yes
 
    - name: Enable Docker Test repo
      ini_file:
        dest: /etc/yum.repos.d/docer-ce.repo
        section: 'docker-ce-test'
        option: enabled
        value: 0
      become: yes

    - name: Install Docker
      package:
        name:
          - docker-ce
          - docker-ce-cli
          - containerd.io
        state: latest
      become: yes

    - name: Docker, Container service Enable
      command: sudo systemctl enable docker containerd

    - name: Docker, Container service Start
      command: sudo systemctl start docker containerd
      
    - name: Add user vagrant to docker group
      user:
        name: vagrant
        groups: docker
        append: yes
      become: yes

    - name: Build nginx 
      shell: |
             sudo echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
             sudo iptables -F 
             sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
             sudo sysctl --system
             mkdir .kube
             sudo sed -i 's/disabled_plugins/#disabled_plugins/g' /etc/containerd/config.toml
             sudo modprobe br_netfilter
             sudo sysctl --system

    - name: Docker, Container service restart
      command: sudo systemctl restart docker containerd

############################### Install Kubernetes ###################################

    - name: Create a kubernetes repo file
      file:
        path: "/etc/yum.repos.d/kubernetes.repo"
        state: "touch"

    - name: Write repo information in kubernetes repo file
      blockinfile:
        path: "/etc/yum.repos.d/kubernetes.repo"
        block: |
               [kubernetes]
               name=Kubernetes
               baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
               enabled=1
               gpgcheck=1
               repo_gpgcheck=1
               gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

    - name: Install Kubernetes
      yum: 
        name: "{{ packages }}"
        state: present
        update_cache: yes
      vars:
        packages:
          - kubelet 
          - kubeadm 
          - kubectl
          - kubernetes-cni
        state: latest

    - name: Kubelet services enable
      command: sudo systemctl enable kubelet

    - name: Init Kubernetes 
      shell: |
             sudo kubeadm init --apiserver-advertise-address=192.168.8.10 --pod-network-cidr=192.168.0.0/16
             sudo cp -i /etc/kubernetes/admin.conf .kube/config
             sudo chown $(id -u):$(id -g) .kube/config
             sudo systemctl start kubelet
             sudo sleep 30
             sudo systemctl restart kubelet

    - name: CNI start in Kubernetes tigera-operator
      command: sudo kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.4/manifests/tigera-operator.yaml

    - name: CNI start in Kubernetes custom-resources
      command: sudo kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.4/manifests/custom-resources.yaml

    - name: Generate join command
      command: kubeadm token create --print-join-command
      register: join_command

    - name: Copy join command to local file
      local_action: copy content="{{ join_command.stdout_lines[0] }}" dest="./join-command"
