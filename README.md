# Create 1 master and 2 worker kubernetes cluster using vagrant

## Let get started

## Vagrant VM provisioning

```
Vagrant.configure("2") do |config|
  config.ssh.username = 'root'
  config.ssh.password = 'vagrant'
  config.ssh.insert_key = 'true'
  config.vm.define "k8s-master" do |master|
    master.vm.box = "centos/7"
    master.vm.network "public_network", ip: "192.168.8.10"
    master.vm.hostname = "k8s-master"
    master.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"
        vb.cpus = "4"
    end
    master.vm.provision "ansible" do |ansible|
        ansible.playbook = "kubernetes-setup/master.yml"
    end
  end
  config.vm.define "k8s-worker01" do |worker|
    worker.vm.box = "centos/7"
    worker.vm.network "public_network", ip: "192.168.8.11"
    worker.vm.hostname = "k8s-worker01"
    worker.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"
        vb.cpus = "4"
    end
    worker.vm.provision "ansible" do |ansible|
        ansible.playbook = "kubernetes-setup/worker.yml"
    end
  end
  config.vm.define "k8s-worker02" do |worker|
    worker.vm.box = "centos/7"
    worker.vm.network "public_network", ip: "192.168.8.12"
    worker.vm.hostname = "k8s-worker02"
    worker.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"
        vb.cpus = "4"
    end
    worker.vm.provision "ansible" do |ansible|
        ansible.playbook = "kubernetes-setup/worker.yml"
    end
  end
end
```

## Kubernetes master node initialization.

```
# loging vagrant box
vagrant ssh k8s-master

su - root

kubeadm init --apiserver-advertise-address=192.168.8.10 --pod-network-cidr=192.168.0.0/16
cp -i /etc/kubernetes/admin.conf .kube/config
sudo chown $(id -u):$(id -g) .kube/config
systemctl start kubelet && sudo sleep 30 && systemctl restart kubelet

```

## CNI deployed tigera-operator and custom-resources

```
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.4/manifests/tigera-operator.yaml

kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.4/manifests/custom-resources.yaml

## Generate token for add worker node

kubeadm token create --print-join-command

watch kubectl get pods --all-namespaces -o wide

```

## Kubernetes worker node joing to master node.

kubeadm join --token <encrept code> 192.168.8.10:6443

## Kubernetes dashboard config in master node.

```
# Create deploy run bash script below

cd dashboard

sh run-create.sh

# Delete deploy run bash script below

sh run-delete.sh
